import sqlite3


def data_logger(user_id_: str, user_command: str, bot_answer: str) -> None:
    """Функция при вызове, принимает на вход:
        chat_id пользователя
        введенную пользователем комманду
        результат выполнения команды
    объеденяет полученные данные в кортеж, соединяется с базой данных(при отсутсвии файла базы данных создает его)
     и записывает данные"""

    datalog = (user_id_, user_command, bot_answer)
    with sqlite3.connect('temp/data.db') as db:
        cursor = db.cursor()
        cursor.execute("""CREATE TABLE IF NOT EXISTS users(data_time, user_id, command, answer)""")
        db.commit()
        cursor.execute(
            """INSERT INTO users VALUES( datetime('now','localtime'), ?, ?, ?)""", datalog)
        db.commit()


def db_search(user_id_: int) -> str:
    """Функция на вход получает chat_id пользователя и осуществляет поиск в бд сообщений связанных с этим chat_id
    Функция возвращает строку, в которой содеражатся все команды вводимые пользователем"""

    with sqlite3.connect('temp/data.db') as db:
        cursor = db.cursor()

        cursor.execute("""SELECT * FROM users WHERE user_id=?""", [(user_id_)])
        result = cursor.fetchall()
        result_ = [f'{item[0]} - {item[2]} - {item[3]}\n' for item in result]
        answer = '\n'.join(result_)
    return answer


def hotel_count_safe(user_id_: str, user_data: str) -> None:
    datalog = (user_id_, user_data)
    with sqlite3.connect('temp/data.db') as db:
        cursor = db.cursor()
        cursor.execute("""CREATE TABLE IF NOT EXISTS user_data(user_id, user_data)""")
        db.commit()
        cursor.execute(
            """INSERT INTO user_data VALUES(?, ?)""", datalog)
        db.commit()


def hotel_count_ref(user_id_: int) -> str:
    with sqlite3.connect('temp/data.db') as db:
        cursor = db.cursor()

        cursor.execute("""SELECT * FROM user_data WHERE user_id=?""", [(user_id_)])
        result = cursor.fetchall()
        result_ = result[len(result) - 1][1]
    return result_
