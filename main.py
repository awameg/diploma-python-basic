import telebot
from decouple import config
from botrequests import lowprice, highprice, bestdeal
from data_logger import data_logger, db_search, hotel_count_safe, hotel_count_ref
import logging

logger = telebot.logger
telebot.logger.setLevel(logging.DEBUG)

API_KEY = config('token')


def telegram_bot(token: str):
    bot = telebot.TeleBot(token)

    @bot.message_handler(commands=['start', 'hello-world'])
    def send_welcome(message) -> None:
        """Реакция на команду /start, /hello-world"""

        send = bot.send_message(message.chat.id, "Привет, я бот Геннадий, для справки введите /help")
        data_logger(message.chat.id, message.text, send.text)

    @bot.message_handler(commands=['help'])
    def send_help(message) -> None:
        """Реакция на команду /help"""

        send = bot.send_message(message.chat.id, "Привет, я бот Геннадий, я знаю следующие комманды:\n"
                                                 "/lowprice - узнать топ самых дешёвых отелей в городе\n"
                                                 "/highprice - узнать топ самых дорогих отелей в городе\n"
                                                 "/bestdeal - узнать топ отелей, наиболее подходящих по цене"
                                                 " и расположению от центра\n"
                                                 "/history - узнать историю поиска отелей")

        data_logger(message.chat.id, message.text, send.text)

    @bot.message_handler(commands=['lowprice'])
    def send_lowhotels(message) -> None:
        """Реакция на команду /lowprice. Запрашивается город и передается в функцию hotels_collect"""

        send = bot.send_message(message.chat.id, 'Введите название города на латинице')
        data_logger(message.chat.id, message.text, send.text)
        bot.register_next_step_handler(send, hotels_collect)

    def hotels_collect(message) -> None:
        """Запрашивается id города на rapidapi для поиска отелей. Запрашивается количество отелей и передается в
        функцию hotels_count"""

        try:
            data_logger(message.chat.id, message.text, '')
            bot.send_message(message.chat.id, 'Ищу...')
            hotels = lowprice.lowprice(message.text)
            send = bot.send_message(message.chat.id, 'Введите количество выводимых отелей')
            bot.register_next_step_handler(send, hotels_count, hotels)
        except Exception:
            bot.send_message(message.chat.id, 'Ничего не найдено')

    def hotels_count(message, lst: list) -> None:
        """Функция получает список отелей и запрашивает вывод фотографий, ответ передает в функцию photo_req"""

        try:
            hotel_count = message.text
            hotel_count_safe(message.chat.id, hotel_count)

            hotels, answer = lowprice.hotels_count(lst, int(hotel_count))
            data_logger(message.chat.id, message.text, answer)
            send = bot.send_message(message.chat.id, 'Загрузить фотографии? - y/n')
            bot.register_next_step_handler(send, photo_req, hotels)
        except (IndexError, ValueError):
            bot.send_message(message.chat.id, 'Возможно вы ввели не число или слишком большее число')

    def photo_req(message, lst: list) -> None:
        """Функция получает список ссылок на фото отелей, запрашивает количество фотографий и передает
         в фунцию load_photo"""

        hotel_count = hotel_count_ref(message.chat.id)

        if message.text.lower() == 'n':
            bot.send_message(message.chat.id, 'Ищу...')
            lst, answer = lowprice.hotels_count(lst, int(hotel_count))
            bot.send_message(message.chat.id, answer)
        elif message.text.lower() == 'y':
            try:
                lst, answer = lowprice.hotels_count(lst, int(hotel_count))
                send = bot.send_message(message.chat.id, 'Сколько загрузить фотографий?')
                bot.register_next_step_handler(send, load_photo, lst)
            except (IndexError, ValueError):
                bot.send_message(message.chat.id, '1Возможно вы ввели не число или слишком большее число')
        else:
            bot.send_message(message.chat.id, 'Ошибка ввода')

    def load_photo(message, lst: list) -> None:
        """Функция отправляет ссылки на фото пользователю"""

        try:
            bot.send_message(message.chat.id, 'Гружу...')
            hotels_photo = lowprice.load_photo(lst, int(message.text))

            for item in hotels_photo:
                bot.send_message(message.chat.id,
                                 f'<a href="{item["url"]}">{item["name"]} - ${item["price"]}</a>',
                                 parse_mode="HTML")
        except (IndexError, ValueError):
            bot.send_message(message.chat.id, 'Возможно вы ввели не число или слишком большее число')

    @bot.message_handler(commands=['highprice'])
    def send_highhotels(message) -> None:
        """Реакция на команду /highprice. Запрашивается город и передается в функцию hotels_collect_high"""

        send = bot.send_message(message.chat.id, 'Введите название города на латинице')
        data_logger(message.chat.id, message.text, send.text)
        bot.register_next_step_handler(send, hotels_collect_high)

    def hotels_collect_high(message) -> None:
        """Запрашивается id города на rapidapi для поиска отелей. Запрашивается количество отелей и передается в
                функцию hotels_count_high"""

        try:
            data_logger(message.chat.id, message.text, '')
            bot.send_message(message.chat.id, 'Ищу...')
            hotels = highprice.highprice(message.text)
            send = bot.send_message(message.chat.id, 'Введите количество выводимых отелей')
            bot.register_next_step_handler(send, hotels_count_high, hotels)
        except Exception:
            bot.send_message(message.chat.id, 'Ничего не найдено')

    def hotels_count_high(message, lst: list) -> None:
        """Функция получает список отелей и запрашивает вывод фотографий, ответ передает в функцию photo_req_high"""

        try:
            hotel_count = message.text
            hotel_count_safe(message.chat.id, hotel_count)

            hotels, answer = highprice.hotels_count(lst, int(hotel_count))
            data_logger(message.chat.id, message.text, answer)
            send = bot.send_message(message.chat.id, 'Загрузить фотографии? - y/n')
            bot.register_next_step_handler(send, photo_req_high, hotels)
        except (IndexError, ValueError):
            bot.send_message(message.chat.id, 'Возможно вы ввели не число или слишком большее число')

    def photo_req_high(message, lst: list) -> None:
        """Функция получает список ссылок на фото отелей, запрашивает количество фотографий и передает
         в фунцию load_photo_high"""

        hotel_count = hotel_count_ref(message.chat.id)

        if message.text.lower() == 'n':
            bot.send_message(message.chat.id, 'Ищу...')
            lst, answer = highprice.hotels_count(lst, int(hotel_count))
            bot.send_message(message.chat.id, answer)
        elif message.text.lower() == 'y':
            try:
                lst, answer = highprice.hotels_count(lst, int(hotel_count))
                send = bot.send_message(message.chat.id, 'Сколько загрузить фотографий?')
                bot.register_next_step_handler(send, load_photo_high, lst)
            except (IndexError, ValueError):
                bot.send_message(message.chat.id, '1Возможно вы ввели не число или слишком большее число')
        else:
            bot.send_message(message.chat.id, 'Ошибка ввода')

    def load_photo_high(message, lst: list) -> None:
        """Функция отправляет ссылки на фото пользователю"""

        try:
            bot.send_message(message.chat.id, 'Гружу...')
            hotels_photo = highprice.load_photo(lst, int(message.text))

            for item in hotels_photo:
                bot.send_message(message.chat.id,
                                 f'<a href="{item["url"]}">{item["name"]} - ${item["price"]}</a>',
                                 parse_mode="HTML")
        except (IndexError, ValueError):
            bot.send_message(message.chat.id, 'Возможно вы ввели не число или слишком большее число')

    @bot.message_handler(commands=['bestdeal'])
    def send_hotels(message) -> None:
        """Реакция на команду /bestdeal. Запрашивается город и передается в функцию hotels_collect_best"""

        send = bot.send_message(message.chat.id, 'Введите название города на латинице')
        data_logger(message.chat.id, message.text, send.text)
        bot.register_next_step_handler(send, hotels_collect_best)

    def hotels_collect_best(message) -> None:
        """Функция запрашивает диапазон цен и передает в hotels_price_best"""

        try:
            data_logger(message.chat.id, message.text, '')
            bot.send_message(message.chat.id, 'Ищу...')
            hotels = bestdeal.bestdeal(message.text)
            send = bot.send_message(message.chat.id, 'Введите диапазон цен через дифис, например: 20-50')
            bot.register_next_step_handler(send, hotels_price_best, hotels)
        except Exception:
            bot.send_message(message.chat.id, 'Ничего не найдено')

    def hotels_price_best(message, lst: list) -> None:
        """Функция запрашивает диапазон расстояний и передает в hotels_price_distance"""

        try:
            bot.send_message(message.chat.id, 'Ищу...')
            hotels = bestdeal.hotel_price(lst, message.text)
            send = bot.send_message(message.chat.id, 'Введите диапазон расстояний от центра, например: 0.5-3')
            bot.register_next_step_handler(send, hotels_price_distance, hotels)
        except Exception:
            bot.send_message(message.chat.id, 'Ничего не найдено')

    def hotels_price_distance(message, lst: list) -> None:
        """Запрашивается количество отелей и передается в функцию hotels_count_best"""

        try:
            bot.send_message(message.chat.id, 'Ищу...')
            hotels = bestdeal.hotel_distance(lst, message.text)
            send = bot.send_message(message.chat.id, 'Введите количество выводимых отелей')
            bot.register_next_step_handler(send, hotels_count_best, hotels)
        except Exception:
            bot.send_message(message.chat.id, 'Ничего не найдено')

    def hotels_count_best(message, lst: list) -> None:
        """Функция получает список отелей и запрашивает вывод фотографий, ответ передает в функцию photo_req_best"""

        try:
            hotel_count = message.text
            hotel_count_safe(message.chat.id, hotel_count)

            hotels, answer = bestdeal.hotels_count(lst, int(hotel_count))
            data_logger(message.chat.id, message.text, answer)
            send = bot.send_message(message.chat.id, 'Загрузить фотографии? - y/n')
            bot.register_next_step_handler(send, photo_req_best, hotels)
        except (IndexError, ValueError):
            bot.send_message(message.chat.id, 'Возможно вы ввели не число или слишком большее число')

    def photo_req_best(message, lst: list) -> None:
        """Функция получает список ссылок на фото отелей, запрашивает количество фотографий и передает
        в фунцию load_photo_best"""

        hotel_count = hotel_count_ref(message.chat.id)

        if message.text.lower() == 'n':
            bot.send_message(message.chat.id, 'Ищу...')
            lst, answer = bestdeal.hotels_count(lst, int(hotel_count))
            bot.send_message(message.chat.id, answer)
        elif message.text.lower() == 'y':
            try:
                lst, answer = bestdeal.hotels_count(lst, int(hotel_count))
                send = bot.send_message(message.chat.id, 'Сколько загрузить фотографий?')
                bot.register_next_step_handler(send, load_photo_best, lst)
            except (IndexError, ValueError):
                bot.send_message(message.chat.id, 'Возможно вы ввели не число или слишком большее число')
        else:
            bot.send_message(message.chat.id, 'Ошибка ввода')

    def load_photo_best(message, lst: list) -> None:
        """Функция отправляет ссылки на фото пользователю"""

        try:
            bot.send_message(message.chat.id, 'Гружу...')
            hotels_photo = bestdeal.load_photo(lst, int(message.text))

            for item in hotels_photo:
                bot.send_message(message.chat.id,
                                 f'<a href="{item["url"]}">{item["name"]} - '
                                 f'{str(item["distance to center"])} мили - ${item["price"]}</a>',
                                 parse_mode="HTML")
        except (IndexError, ValueError):
            bot.send_message(message.chat.id, 'Возможно вы ввели не число или слишком большее число')

    @bot.message_handler(commands=['history'])
    def send_history(message) -> None:
        """Реакция на команду /history. Ищет в базе сообщения по chat_id пользователя и отправляет их сообщением"""

        try:
            send = db_search(message.chat.id)
            if len(send) > 4096:
                for x in range(0, len(send), 4096):
                    bot.send_message(message.chat.id, send[x:x + 4096])
            else:
                bot.send_message(message.chat.id, send)
        except Exception:
            bot.send_message(message.chat.id, 'Ничего не найдено')

    @bot.message_handler(content_types=['text'])
    def send_text(message) -> None:
        """Реакция на сообщение 'привет' """

        if message.text.lower() == 'привет':
            bot.send_message(message.chat.id, "Привет, я бот Геннадий")

    bot.infinity_polling(timeout=10, long_polling_timeout=60)


if __name__ == '__main__':
    telegram_bot(API_KEY)
